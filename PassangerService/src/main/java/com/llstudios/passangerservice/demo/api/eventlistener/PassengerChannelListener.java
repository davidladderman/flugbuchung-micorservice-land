package com.llstudios.passangerservice.demo.api.eventlistener;

import com.llstudios.passangerservice.demo.infrastructure.messagebroker.PassengerChannels;
import com.llstudios.passangerservice.demo.shareddomain.events.PassengerCreatedEvent;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.annotation.StreamListener;

//Horcht auf den MessageBroker ob er was bekommen hat
@EnableBinding
@Slf4j
public class PassengerChannelListener {
    //wenn rabbitmwq eine passenger creation sink aufruft wird es mit stream listener gefangen und kann sommit weiter funktionen aufrufen hier wird jetzt nur ein console log ausgegeben
    @StreamListener(target = PassengerChannels.PASSENGER_CREATION_CHANNEL_SINK)
    public void processPassengerCreated(PassengerCreatedEvent passengerCreatedEvent){
        log.info("Event vom MessageBroker gefangen" + passengerCreatedEvent.getPassengerCreatedEventData());
    }
}
