package com.llstudios.passangerservice.demo.api.rest.dto;

import lombok.AllArgsConstructor;
import lombok.Value;


public class CreatePassengerDto {
    String firstName;
    String lastName;
    String postleitzahl;
    String ort;
    String passnummer;

    public CreatePassengerDto() {
    }


    public CreatePassengerDto(String firstName, String lastName, String postleitzahl, String ort, String passnummer) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.postleitzahl = postleitzahl;
        this.ort = ort;
        this.passnummer = passnummer;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getPostleitzahl() {
        return postleitzahl;
    }

    public String getOrt() {
        return ort;
    }

    public String getPassnummer() {
        return passnummer;
    }
}
