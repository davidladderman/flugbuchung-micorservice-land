package com.llstudios.passangerservice.demo.domain.commands;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Value;

@Value
@AllArgsConstructor
@Builder
public class CreatePassangerCommand {
    private String firstName;
    private String lastName;
    private String postleitzahl;
    private String ort;
    private String passnummer;
}
