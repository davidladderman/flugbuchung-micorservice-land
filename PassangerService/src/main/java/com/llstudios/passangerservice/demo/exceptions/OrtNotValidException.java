package com.llstudios.passangerservice.demo.exceptions;


import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.EXPECTATION_FAILED)
public class OrtNotValidException extends Exception {

    public OrtNotValidException() {
        super("Ort not valid");
    }
}
