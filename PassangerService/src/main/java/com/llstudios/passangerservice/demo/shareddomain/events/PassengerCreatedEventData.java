package com.llstudios.passangerservice.demo.shareddomain.events;

import lombok.Data;

@Data
public class PassengerCreatedEventData {
    private String firstName;
    private String lastName;
    private String postleitzahl;
    private String ort;
    private String passnummer;

    public PassengerCreatedEventData(String firstName, String lastName, String postleitzahl, String ort, String passnummer) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.postleitzahl = postleitzahl;
        this.ort = ort;
        this.passnummer = passnummer;
    }

    public PassengerCreatedEventData() {
    }
}
