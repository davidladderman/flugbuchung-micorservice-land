package com.llstudios.passangerservice.demo.exceptions;


import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.EXPECTATION_FAILED)
public class NameNotValidException extends Exception {

    public NameNotValidException() {
        super("Invalid name. Name must not be empty, Name must have at least one character.");
    }
}
