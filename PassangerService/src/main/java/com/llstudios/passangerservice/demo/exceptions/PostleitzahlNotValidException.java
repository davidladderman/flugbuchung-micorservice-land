package com.llstudios.passangerservice.demo.exceptions;


import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.EXPECTATION_FAILED)
public class PostleitzahlNotValidException extends Exception {

    public PostleitzahlNotValidException() {
        super("Invalid Postleitzahl must have 4 numbers");
    }
}
