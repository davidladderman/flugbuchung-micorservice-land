package com.llstudios.passangerservice.demo.domain.valueobjects;

import com.llstudios.passangerservice.demo.exceptions.PostleitzahlNotValidException;
import lombok.EqualsAndHashCode;
import lombok.Getter;

import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Embeddable
@Getter
@EqualsAndHashCode
public class Postleitzahl {
    @NotNull
    @Size(min = 4, max = 4)
    private String postleitzahl = "0000";

    public Postleitzahl(String postleitzahl) throws PostleitzahlNotValidException{
        if (postleitzahl.matches("^([0-9]{4})")) {
            this.postleitzahl = postleitzahl;
        } else {
            throw new PostleitzahlNotValidException();
        }
    }

    public Postleitzahl() {

    }
}
