package com.llstudios.passangerservice.demo.shareddomain.events;

import com.llstudios.passangerservice.demo.domain.aggregates.Passenger;
import lombok.Data;

@Data
public class PassengerCreatedEvent {
    private PassengerCreatedEventData passengerCreatedEventData;

    public PassengerCreatedEvent() {
    }

    public PassengerCreatedEvent(PassengerCreatedEventData passengerCreatedEventData) {
        this.passengerCreatedEventData = passengerCreatedEventData;
    }
}
