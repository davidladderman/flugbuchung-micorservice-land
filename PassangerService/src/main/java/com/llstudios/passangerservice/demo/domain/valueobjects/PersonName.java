package com.llstudios.passangerservice.demo.domain.valueobjects;

import com.llstudios.passangerservice.demo.exceptions.NameNotValidException;
import lombok.EqualsAndHashCode;
import lombok.Getter;

import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Embeddable
@Getter
@EqualsAndHashCode
public class PersonName {
    @NotNull
    @Size(min = 2)
    private String name = "DEFAULT";



    public PersonName(String name) throws NameNotValidException{
        if (name != null && name != "" && name.length() > 1) {
            this.name = name;
        } else {
            throw new NameNotValidException();
        }
    }

    public PersonName() {

    }
}
