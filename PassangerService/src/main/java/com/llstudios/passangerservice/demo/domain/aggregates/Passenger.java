package com.llstudios.passangerservice.demo.domain.aggregates;

import com.llstudios.passangerservice.demo.domain.commands.CreatePassangerCommand;
import com.llstudios.passangerservice.demo.domain.valueobjects.Ort;
import com.llstudios.passangerservice.demo.domain.valueobjects.Passnummer;
import com.llstudios.passangerservice.demo.domain.valueobjects.PersonName;
import com.llstudios.passangerservice.demo.domain.valueobjects.Postleitzahl;
import com.llstudios.passangerservice.demo.exceptions.NameNotValidException;
import com.llstudios.passangerservice.demo.exceptions.OrtNotValidException;
import com.llstudios.passangerservice.demo.exceptions.PostleitzahlNotValidException;
import com.llstudios.passangerservice.demo.shareddomain.events.PassengerCreatedEvent;
import com.llstudios.passangerservice.demo.shareddomain.events.PassengerCreatedEventData;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.springframework.data.domain.AbstractAggregateRoot;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Getter
@EqualsAndHashCode

public class Passenger extends AbstractAggregateRoot<Passenger> {
    @Id
    @GeneratedValue
    private Long id;

    public Passenger() {
    }

    @Embedded
    @NotNull
    @AttributeOverrides({
            @AttributeOverride(name = "name", column = @Column(name = "firstName"))
    })
    private PersonName firstName;

    @Embedded
    @NotNull
    @AttributeOverrides({
            @AttributeOverride(name = "name", column = @Column(name = "lastName"))
    })
    private PersonName lastName;

    @Embedded
    @NotNull
    private Postleitzahl postleitzahl;

    @Embedded
    @NotNull
    private Ort ort;

    @Embedded
    @Column(unique = true)
    private Passnummer passnummer;

    public Passenger(CreatePassangerCommand createPassangerCommand) throws NameNotValidException, PostleitzahlNotValidException, OrtNotValidException {
        this.firstName = new PersonName(createPassangerCommand.getFirstName());
        this.lastName = new PersonName(createPassangerCommand.getLastName());
        this.postleitzahl = new Postleitzahl(createPassangerCommand.getPostleitzahl());
        this.ort = new Ort(createPassangerCommand.getOrt());
        this.passnummer = new Passnummer(createPassangerCommand.getPassnummer());
        addPassengerEvent(new PassengerCreatedEvent(
                new PassengerCreatedEventData(
                        this.firstName.getName(),
                        this.lastName.getName(),
                        this.postleitzahl.getPostleitzahl(),
                        this.ort.getOrt(),
                        this.passnummer.getPassnummer()
                )
        ));

    }

    //Sobald in der Datenbank gespeichert wird wird ein internes Event gefeuert.
    public void addPassengerEvent(Object event){
        registerEvent(event);
    }
}
