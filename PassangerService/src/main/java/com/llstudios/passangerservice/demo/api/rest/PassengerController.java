package com.llstudios.passangerservice.demo.api.rest;

import com.llstudios.passangerservice.demo.api.rest.dto.CreatePassengerDto;
import com.llstudios.passangerservice.demo.domain.aggregates.Passenger;
import com.llstudios.passangerservice.demo.exceptions.NameNotValidException;
import com.llstudios.passangerservice.demo.exceptions.OrtNotValidException;
import com.llstudios.passangerservice.demo.exceptions.PostleitzahlNotValidException;
import com.llstudios.passangerservice.demo.infrastructure.repo.PassengerRepository;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;

import static com.llstudios.passangerservice.demo.api.rest.mapper.PassengerDTOtoPassengerCommandMapper.toCreatePassangerCommand;

@RestController
@RequestMapping("/passengers")
public class PassengerController {
    private final PassengerRepository passengerRepository;

    public PassengerController(PassengerRepository passengerRepository) {
        this.passengerRepository = passengerRepository;
    }


    //greift direkt auf Repository zu service LAyer wird ausgelassen da keine Businesslogik sondern nur CRUD
    @PostMapping
    public ResponseEntity<?> createPassenger(@RequestBody CreatePassengerDto createPassengerDto) throws PostleitzahlNotValidException, NameNotValidException, OrtNotValidException {
        System.out.println("POST MAPPING create Passenger");
        Passenger passenger = new Passenger(toCreatePassangerCommand(createPassengerDto));
        passengerRepository.save(passenger);//das interne domain event wird getriggert
        return ResponseEntity.status(201).build();
    }
}
