package com.llstudios.passangerservice.demo.infrastructure.repo;

import com.llstudios.passangerservice.demo.domain.aggregates.Passenger;
import com.llstudios.passangerservice.demo.domain.valueobjects.PersonName;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface PassengerRepository extends JpaRepository<Passenger, Long> {
    List<Passenger> findByFirstName(PersonName firstName);
    List<Passenger> findByLastName(PersonName lastName);
}
