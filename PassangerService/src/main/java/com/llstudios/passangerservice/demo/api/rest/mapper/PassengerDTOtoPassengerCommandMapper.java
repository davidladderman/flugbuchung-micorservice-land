package com.llstudios.passangerservice.demo.api.rest.mapper;

import com.llstudios.passangerservice.demo.api.rest.dto.CreatePassengerDto;
import com.llstudios.passangerservice.demo.domain.commands.CreatePassangerCommand;

public class PassengerDTOtoPassengerCommandMapper {
    public static CreatePassangerCommand toCreatePassangerCommand(CreatePassengerDto createPassengerDto){
        return new CreatePassangerCommand(createPassengerDto.getFirstName(), createPassengerDto.getLastName(), createPassengerDto.getPostleitzahl(), createPassengerDto.getOrt(), createPassengerDto.getPassnummer());
    }
}
