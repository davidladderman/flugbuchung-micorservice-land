package com.llstudios.passangerservice.demo.services;

import com.llstudios.passangerservice.demo.infrastructure.messagebroker.PassengerChannels;
import com.llstudios.passangerservice.demo.shareddomain.events.PassengerCreatedEvent;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.event.TransactionalEventListener;

@Service
@EnableBinding(PassengerChannels.class) //Channelklasse  kann jeder Broker ausgetauscht werden
@Slf4j
@AllArgsConstructor
public class PassengerDomainEventAndPubService {
    private final PassengerChannels passengerChannels;

    //fängt das interne Domain Event ab und schickt ein globaeles Event an den Message Broker raus mit Payload
    @TransactionalEventListener
    public void handlePassengerCreatedEvent(PassengerCreatedEvent passengerCreatedEvent){
        try{
        log.info("Es wurde auf internes Domain Event gehorcht (.save methode im repository wurde ausgeführt)" + passengerCreatedEvent.getPassengerCreatedEventData());
        log.info("Eventdaten werden weiter geschickt an messagebroker RabbitMQ");
        passengerChannels.passengerCreationSource().send(MessageBuilder.withPayload(passengerCreatedEvent).build());}
        catch (Exception e){
            log.error("Keine Verbindung zum Messagebroker");
            e.printStackTrace();
        }
    }
}
