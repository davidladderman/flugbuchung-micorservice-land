package com.llstudios.passangerservice.demo.domain.valueobjects;

import com.llstudios.passangerservice.demo.exceptions.OrtNotValidException;
import lombok.EqualsAndHashCode;
import lombok.Getter;

import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Embeddable
@Getter
@EqualsAndHashCode
public class Ort {
    @NotNull
    private String ort = "Ort";

    public Ort(String ort) throws OrtNotValidException{
        if(ort != null && ort != ""){
            this.ort = ort;
        }else {
            throw new OrtNotValidException();
        }
    }

    public Ort() {

    }
}
