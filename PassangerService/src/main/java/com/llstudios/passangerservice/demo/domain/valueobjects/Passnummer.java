package com.llstudios.passangerservice.demo.domain.valueobjects;

import com.llstudios.passangerservice.demo.exceptions.NameNotValidException;
import lombok.EqualsAndHashCode;
import lombok.Getter;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Embeddable
@Getter
@EqualsAndHashCode
public class Passnummer {
    @NotNull
    @Size(min = 10, max = 10)
    @Column(unique = true)
    private String passnummer = "0000000000";

    public Passnummer(String passnummer) throws NameNotValidException {
        if (passnummer.matches("[A-Z0-9]{10}")) {
            this.passnummer = passnummer;
        } else {
            throw new NameNotValidException();
        }
    }

    public Passnummer() {

    }
}
