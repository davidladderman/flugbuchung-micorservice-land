package com.llstudios.flightbookingservice.services;

import com.llstudios.flightbookingservice.domain.booking.aggregates.Booking;
import com.llstudios.flightbookingservice.domain.booking.commands.CreateBookingCommand;
import com.llstudios.flightbookingservice.domain.flight.ValueObjects.Cost;
import com.llstudios.flightbookingservice.domain.flight.aggregates.Flight;
import com.llstudios.flightbookingservice.domain.passenger.ValueObjects.Geld;
import com.llstudios.flightbookingservice.domain.passenger.aggregates.Passenger;
import com.llstudios.flightbookingservice.domain.passenger.commands.DecreasePassengerMoneyCommand;
import com.llstudios.flightbookingservice.exceptions.*;
import com.llstudios.flightbookingservice.infrastructure.repo.BookingRepository;
import com.llstudios.flightbookingservice.infrastructure.repo.FlightRepository;
import com.llstudios.flightbookingservice.infrastructure.repo.PassengerRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;

@Service
@AllArgsConstructor
public class BookingService {
    BookingRepository bookingRepository;
    PassengerRepository passengerRepository;
    FlightRepository flightRepository;
    PassengerService passengerService;

    public void createBooking(CreateBookingCommand createBookingCommand) throws AlreadyBookedException, FlightNotFoundException, PassengerNotFoundEsception, NoFlightCostException, SieSindArmException {
        if(bookingRepository.findByBookingnummer(createBookingCommand.getBookingnummer()).isPresent()){
            throw new AlreadyBookedException();
        }

        Flight flight = flightRepository.findByFlightNumber(createBookingCommand.getFlugnummer()).orElseThrow(()-> new FlightNotFoundException());
        Passenger passenger = passengerRepository.findByPassnummer(createBookingCommand.getPassnummer()).orElseThrow(()->new PassengerNotFoundEsception());


        if(flight.getFlightCost().getCost().equals(new BigDecimal("0"))){
            throw new NoFlightCostException();
        }

        if (bookingAffordableForPassenger(passenger,flight)){
            BigDecimal flugkosten = flight.getFlightCost().getCost();
            System.out.println("Flugkosten: " + flugkosten.toString());
            BigDecimal passengergeld = passenger.getPassengerGeld().getGeld();
            passengerService.decreaseMoneyMoneyForStudent(new DecreasePassengerMoneyCommand(passenger.getPassnummer(),flugkosten.toString()));
            Booking booking = new Booking(createBookingCommand);
            bookingRepository.save(booking);
        }else {
            throw new SieSindArmException();
        }

    }

    private boolean bookingAffordableForPassenger(Passenger passenger, Flight flight){
        int result = passenger.getPassengerGeld().getGeld().compareTo(flight.getFlightCost().getCost());
        System.out.println("Result os from compare" + result);
        if (result == 1 || result == 0){
            System.out.println("GEHT SICH AUS");
            return true;
        }
        else {
            return false;
        }
    }

  /*  private BigDecimal calculateFlightCost(Flight flight){
        BigDecimal flightcost = new BigDecimal(0.0);
        for(Cost cost : flight.getFlightCostList()){
            flightcost.add(cost.getCost());
        }
        return flightcost;
    }

    private BigDecimal calculatePassengerMoney(Passenger passenger){
        BigDecimal passengerMoney = new BigDecimal(0.0);
        for(Geld geld : passenger.getUserGeldListe()){
            passengerMoney.add(geld.getGeld());
        }
        return passengerMoney;
    }
*/

}
