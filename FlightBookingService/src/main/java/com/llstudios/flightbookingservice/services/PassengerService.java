package com.llstudios.flightbookingservice.services;

import com.llstudios.flightbookingservice.domain.passenger.aggregates.Passenger;
import com.llstudios.flightbookingservice.domain.passenger.commands.AddMoneyCommand;
import com.llstudios.flightbookingservice.domain.passenger.commands.CreatePassengerCommand;
import com.llstudios.flightbookingservice.domain.passenger.commands.DecreasePassengerMoneyCommand;
import com.llstudios.flightbookingservice.exceptions.PassengerNotFoundEsception;
import com.llstudios.flightbookingservice.infrastructure.repo.PassengerRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class PassengerService {
    private PassengerRepository passengerRepository;

    public String createPassenger(CreatePassengerCommand createPassengerCommand){
        Passenger passenger = new Passenger(createPassengerCommand);
        passengerRepository.findByPassnummer(passenger.getPassnummer()).orElse(passengerRepository.save(passenger));
        return passenger.getPassnummer();
    }

    public void addMoneyMoneyForStudent(AddMoneyCommand addMoneyCommand) throws PassengerNotFoundEsception {
        Passenger passenger = passengerRepository.findByPassnummer(addMoneyCommand.getPassnummer()).orElseThrow(()-> new PassengerNotFoundEsception());
        passenger.addMoneyBudget(addMoneyCommand);
        passengerRepository.save(passenger);
    }

    public void decreaseMoneyMoneyForStudent(DecreasePassengerMoneyCommand decreasePassengerMoneyCommand) throws PassengerNotFoundEsception {
        Passenger passenger = passengerRepository.findByPassnummer(decreasePassengerMoneyCommand.getPassnummer()).orElseThrow(()-> new PassengerNotFoundEsception());

        passenger.decreaseMoneyBudget(decreasePassengerMoneyCommand);
        passengerRepository.save(passenger);
    }
}
