package com.llstudios.flightbookingservice.services;

import com.llstudios.flightbookingservice.domain.flight.aggregates.Flight;
import com.llstudios.flightbookingservice.exceptions.FlightNotFoundException;
import com.llstudios.flightbookingservice.domain.flight.commands.AddCostCommand;
import com.llstudios.flightbookingservice.domain.flight.commands.CreateFlightCommand;
import com.llstudios.flightbookingservice.infrastructure.repo.FlightRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.text.ParseException;

@Service
@AllArgsConstructor
public class FlightService {
    private FlightRepository flightRepository;

    public String createFlight(CreateFlightCommand createFlightCommand) throws ParseException {
        Flight flight = new Flight(createFlightCommand);
        flightRepository.findByFlightNumber(flight.getFlightNumber()).orElse(flightRepository.save(flight));
        return flight.getFlightNumber();
    }

    public void addCostToFlight(AddCostCommand addCostCommand) throws FlightNotFoundException {
        Flight flight = flightRepository.findByFlightNumber(addCostCommand.getFlightnumber()).orElseThrow(() -> new FlightNotFoundException());
        flight.addCost(addCostCommand);
        flightRepository.save(flight);
    }
}
