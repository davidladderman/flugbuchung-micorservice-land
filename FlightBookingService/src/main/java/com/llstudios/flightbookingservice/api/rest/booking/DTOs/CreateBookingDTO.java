package com.llstudios.flightbookingservice.api.rest.booking.DTOs;

import lombok.AllArgsConstructor;
import lombok.Value;

@Value
@AllArgsConstructor
public class CreateBookingDTO {
    private String bookingnummer;
    private String passnummer;
    private String flugnummer;
}
