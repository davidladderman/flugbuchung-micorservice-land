package com.llstudios.flightbookingservice.api.rest.passenger.DTOs;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
@AllArgsConstructor
public class AddMonDTO {
    String passnummer;
    String geld;
}
