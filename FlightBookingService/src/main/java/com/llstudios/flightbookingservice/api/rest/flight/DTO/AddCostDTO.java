package com.llstudios.flightbookingservice.api.rest.flight.DTO;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
@AllArgsConstructor
public class AddCostDTO {
    String flightnumber;
    String cost;
}
