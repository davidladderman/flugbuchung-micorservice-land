package com.llstudios.flightbookingservice.api.rest.passenger.mapper;

import com.llstudios.flightbookingservice.api.rest.passenger.DTOs.AddMonDTO;
import com.llstudios.flightbookingservice.domain.passenger.commands.AddMoneyCommand;

public class MoneyDTOMapper {

    public static AddMoneyCommand toCommand(AddMonDTO addMonDTO){
        return new AddMoneyCommand(addMonDTO.getPassnummer(), addMonDTO.getGeld());
    }
}
