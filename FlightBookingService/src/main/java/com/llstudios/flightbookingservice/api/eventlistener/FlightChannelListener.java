package com.llstudios.flightbookingservice.api.eventlistener;

import com.llstudios.flightbookingservice.api.eventlistener.mapper.FlightCreatedEventToCommand;
import com.llstudios.flightbookingservice.infrastructure.messagebroker.FlightChannels;
import com.llstudios.flightbookingservice.infrastructure.messagebroker.PassengerChannels;
import com.llstudios.flightbookingservice.services.FlightService;
import com.llstudios.flightbookingservice.shareddomain.FlightCreatedEvent;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.annotation.StreamListener;

import java.text.ParseException;

@EnableBinding(FlightChannels.class)
@Slf4j
@AllArgsConstructor
public class FlightChannelListener {
    private final FlightService flightService;

    @StreamListener(target = FlightChannels.FLIGHT_CREATION_CHANNEL_SINK)
    public void processFlightCreated(FlightCreatedEvent flightCreatedEvent) throws ParseException {
        log.info("Flight received");
        log.info(flightCreatedEvent.toString());
        flightService.createFlight(FlightCreatedEventToCommand.toCommand(flightCreatedEvent));
    }
}
