package com.llstudios.flightbookingservice.api.eventlistener.mapper;

import com.llstudios.flightbookingservice.domain.flight.commands.CreateFlightCommand;
import com.llstudios.flightbookingservice.shareddomain.FlightCreatedEvent;
import com.llstudios.flightbookingservice.shareddomain.FlightCreatedEventData;

public class FlightCreatedEventToCommand {
    public static CreateFlightCommand toCommand(FlightCreatedEvent flightCreatedEvent){
        FlightCreatedEventData flightCreatedEventData = flightCreatedEvent.getFlightCreatedEventData();
        return new CreateFlightCommand(
                flightCreatedEventData.getFlightDate(),
                flightCreatedEventData.getFlightFlugzeitBis(),
                flightCreatedEventData.getFlightFlugzeitVon(),
                flightCreatedEventData.getFlightNumber(),
                flightCreatedEventData.getFlightStartflughafen(),
                flightCreatedEventData.getFlightZielflughafen());
    }
}
