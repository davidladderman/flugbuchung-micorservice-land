package com.llstudios.flightbookingservice.api.rest.booking.mapper;

import com.llstudios.flightbookingservice.api.rest.booking.DTOs.CreateBookingDTO;
import com.llstudios.flightbookingservice.domain.booking.commands.CreateBookingCommand;

public class BookingCommandMapper {
    public static CreateBookingCommand toCreateBookingCommand(CreateBookingDTO createBookingDTO){
        return new CreateBookingCommand(createBookingDTO.getBookingnummer(), createBookingDTO.getPassnummer(), createBookingDTO.getFlugnummer());
    }
}
