package com.llstudios.flightbookingservice.api.eventlistener.mapper;

import com.llstudios.flightbookingservice.domain.passenger.commands.CreatePassengerCommand;
import com.llstudios.flightbookingservice.shareddomain.PassengerCreatedEvent;
import com.llstudios.flightbookingservice.shareddomain.PassengerCreatedEventData;

public class PassengerCreatedEventToCommand {

    public static CreatePassengerCommand toCommand(PassengerCreatedEvent passengerCreatedEvent){
        PassengerCreatedEventData passengerCreatedEventData = passengerCreatedEvent.getPassengerCreatedEventData();
        return new CreatePassengerCommand(passengerCreatedEventData.getFirstName(), passengerCreatedEventData.getLastName(), passengerCreatedEventData.getPostleitzahl(), passengerCreatedEventData.getOrt(), passengerCreatedEventData.getPassnummer());
    }
}
