package com.llstudios.flightbookingservice.api.rest.flight.mapper;

import com.llstudios.flightbookingservice.api.rest.flight.DTO.AddCostDTO;
import com.llstudios.flightbookingservice.domain.flight.commands.AddCostCommand;

public class CostDTOMapper {
    public static AddCostCommand toCommand(AddCostDTO addCostDTO){
        return new AddCostCommand(addCostDTO.getFlightnumber(), addCostDTO.getCost());
    }
}
