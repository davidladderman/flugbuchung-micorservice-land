package com.llstudios.flightbookingservice.api.eventlistener;

import com.llstudios.flightbookingservice.api.eventlistener.mapper.PassengerCreatedEventToCommand;
import com.llstudios.flightbookingservice.infrastructure.messagebroker.PassengerChannels;
import com.llstudios.flightbookingservice.services.PassengerService;
import com.llstudios.flightbookingservice.shareddomain.PassengerCreatedEvent;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.annotation.StreamListener;

@EnableBinding(PassengerChannels.class)
@Slf4j //logger
public class PassengerChannelListener {
    final PassengerService passengerService;

    public PassengerChannelListener(PassengerService passengerService) {
        this.passengerService = passengerService;
    }


    @StreamListener(target = PassengerChannels.PASSENGER_CREATION_CHANNEL_SINK)
    public void processPassengerCreated(PassengerCreatedEvent passengerCreatedEvent){
        log.info("HAT EVENT VON PASSENGER EMPFANGEN");
        passengerService.createPassenger(PassengerCreatedEventToCommand.toCommand(passengerCreatedEvent));
    }
}
