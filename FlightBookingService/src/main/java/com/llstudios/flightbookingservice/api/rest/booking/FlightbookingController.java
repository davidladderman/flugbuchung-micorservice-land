package com.llstudios.flightbookingservice.api.rest.booking;

import com.llstudios.flightbookingservice.api.rest.booking.DTOs.CreateBookingDTO;
import com.llstudios.flightbookingservice.api.rest.booking.mapper.BookingCommandMapper;
import com.llstudios.flightbookingservice.exceptions.*;
import com.llstudios.flightbookingservice.services.BookingService;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@AllArgsConstructor
public class FlightbookingController {
    BookingService bookingService;

    @PostMapping("/bookings")
    public ResponseEntity<?> makeBooking(@RequestBody CreateBookingDTO createBookingDTO) throws NoFlightCostException, PassengerNotFoundEsception, SieSindArmException, FlightNotFoundException, AlreadyBookedException {
            bookingService.createBooking(BookingCommandMapper.toCreateBookingCommand(createBookingDTO));
            return ResponseEntity.status(201).build();
    }

}
