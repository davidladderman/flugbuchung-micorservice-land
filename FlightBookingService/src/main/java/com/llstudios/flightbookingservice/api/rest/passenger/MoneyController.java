package com.llstudios.flightbookingservice.api.rest.passenger;

import com.llstudios.flightbookingservice.api.rest.flight.DTO.AddCostDTO;
import com.llstudios.flightbookingservice.api.rest.flight.mapper.CostDTOMapper;
import com.llstudios.flightbookingservice.api.rest.passenger.DTOs.AddMonDTO;
import com.llstudios.flightbookingservice.api.rest.passenger.mapper.MoneyDTOMapper;
import com.llstudios.flightbookingservice.exceptions.FlightNotFoundException;
import com.llstudios.flightbookingservice.exceptions.PassengerNotFoundEsception;
import com.llstudios.flightbookingservice.services.FlightService;
import com.llstudios.flightbookingservice.services.PassengerService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@AllArgsConstructor
@RequestMapping("/accounting")
public class MoneyController {

    PassengerService passengerService;
    FlightService flightService;

    @PostMapping("/addmoney")
    public void addMoneytoPassenger(@RequestBody AddMonDTO addMoneyDTO) throws PassengerNotFoundEsception {
        System.out.println("GELD WIRD HINZUGEFÜGT");
        passengerService.addMoneyMoneyForStudent(MoneyDTOMapper.toCommand(addMoneyDTO));
    }

    @PostMapping("/addflightcost")
    public void addCostToFlight(@RequestBody AddCostDTO addCostDTO) throws FlightNotFoundException {
        System.out.println("Flightcost added!");
        flightService.addCostToFlight(CostDTOMapper.toCommand(addCostDTO));
    }
}
