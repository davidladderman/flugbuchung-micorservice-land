package com.llstudios.flightbookingservice.infrastructure.repo;

import com.llstudios.flightbookingservice.domain.passenger.aggregates.Passenger;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface PassengerRepository extends JpaRepository<Passenger,Long> {
    Optional<Passenger> findByPassnummer(String passnummer);
}
