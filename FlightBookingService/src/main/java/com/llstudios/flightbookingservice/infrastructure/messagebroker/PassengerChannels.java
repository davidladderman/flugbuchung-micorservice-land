package com.llstudios.flightbookingservice.infrastructure.messagebroker;

import org.springframework.cloud.stream.annotation.Input;
import org.springframework.messaging.MessageChannel;

public interface PassengerChannels {
    String PASSENGER_CREATION_CHANNEL_SINK = "passengerCreationChannelSink";

    @Input(PASSENGER_CREATION_CHANNEL_SINK)
    MessageChannel passengerCreation();
}
