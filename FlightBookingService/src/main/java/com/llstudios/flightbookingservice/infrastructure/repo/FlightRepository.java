package com.llstudios.flightbookingservice.infrastructure.repo;

import com.llstudios.flightbookingservice.domain.flight.aggregates.Flight;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface FlightRepository extends JpaRepository<Flight, Long> {
    Optional<Flight> findByFlightNumber(String flightNumber);
}
