package com.llstudios.flightbookingservice.exceptions;

public class PassengerNotFoundEsception extends Exception{
    public PassengerNotFoundEsception() {
        super("Passenger not found");
    }
}
