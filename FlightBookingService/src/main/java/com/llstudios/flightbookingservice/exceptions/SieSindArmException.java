package com.llstudios.flightbookingservice.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.CONFLICT)
public class SieSindArmException extends Exception{
    public SieSindArmException() {
        super("Leider können sie nicht Fliegen kein geld");
    }
}
