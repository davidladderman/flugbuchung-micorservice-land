package com.llstudios.flightbookingservice.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.CONFLICT)
public class NoFlightCostException extends Exception{
    public NoFlightCostException() {
        super("NO FLIGHT COST MICHAEL.");
    }
}
