package com.llstudios.flightbookingservice.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.CONFLICT)
public class AlreadyBookedException extends Exception{
    public AlreadyBookedException() {
        super("Duplicate booking number exception.");
    }
}
