package com.llstudios.flightbookingservice.shareddomain;

import lombok.Data;

@Data
public class FlightCreatedEvent {
    private FlightCreatedEventData flightCreatedEventData;

    protected FlightCreatedEvent() {
    }

    public FlightCreatedEvent(FlightCreatedEventData flightCreatedEventData) {
        this.flightCreatedEventData = flightCreatedEventData;
    }


}
