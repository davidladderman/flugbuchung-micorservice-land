package com.llstudios.flightbookingservice.shareddomain;

import lombok.Data;
import lombok.Getter;


public class PassengerCreatedEventData {
    private String firstName;
    private String lastName;
    private String postleitzahl;
    private String ort;
    private String passnummer;

    public PassengerCreatedEventData(String firstName, String lastName, String postleitzahl, String ort, String passnummer) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.postleitzahl = postleitzahl;
        this.ort = ort;
        this.passnummer = passnummer;
    }

    public PassengerCreatedEventData() {
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getPostleitzahl() {
        return postleitzahl;
    }

    public String getOrt() {
        return ort;
    }

    public String getPassnummer() {
        return passnummer;
    }
}
