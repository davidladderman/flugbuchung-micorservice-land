package com.llstudios.flightbookingservice.shareddomain;

import lombok.*;

@Data
@NoArgsConstructor
public class FlightCreatedEventData {
    private String flightDate;
    private String flightFlugzeitBis;
    private String flightFlugzeitVon;
    private String flightNumber;
    private String flightStartflughafen;
    private String flightZielflughafen;

    public FlightCreatedEventData(String flightDate, String flightFlugzeitBis, String flightFlugzeitVon, String flightNumber, String flightStartflughafen, String flightZielflughafen) {
        this.flightDate = flightDate;
        this.flightFlugzeitBis = flightFlugzeitBis;
        this.flightFlugzeitVon = flightFlugzeitVon;
        this.flightNumber = flightNumber;
        this.flightStartflughafen = flightStartflughafen;
        this.flightZielflughafen = flightZielflughafen;
    }

    public String getFlightDate() {
        return flightDate;
    }

    public void setFlightDate(String flightDate) {
        this.flightDate = flightDate;
    }

    public String getFlightFlugzeitBis() {
        return flightFlugzeitBis;
    }

    public void setFlightFlugzeitBis(String flightFlugzeitBis) {
        this.flightFlugzeitBis = flightFlugzeitBis;
    }

    public String getFlightFlugzeitVon() {
        return flightFlugzeitVon;
    }

    public void setFlightFlugzeitVon(String flightFlugzeitVon) {
        this.flightFlugzeitVon = flightFlugzeitVon;
    }

    public String getFlightNumber() {
        return flightNumber;
    }

    public void setFlightNumber(String flightNumber) {
        this.flightNumber = flightNumber;
    }

    public String getFlightStartflughafen() {
        return flightStartflughafen;
    }

    public void setFlightStartflughafen(String flightStartflughafen) {
        this.flightStartflughafen = flightStartflughafen;
    }

    public String getFlightZielflughafen() {
        return flightZielflughafen;
    }

    public void setFlightZielflughafen(String flightZielflughafen) {
        this.flightZielflughafen = flightZielflughafen;
    }
}
