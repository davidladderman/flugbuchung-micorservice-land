package com.llstudios.flightbookingservice.shareddomain;

import lombok.Data;


public class PassengerCreatedEvent {
    private PassengerCreatedEventData passengerCreatedEventData;

    public PassengerCreatedEvent() {
    }

    public PassengerCreatedEvent(PassengerCreatedEventData passengerCreatedEventData) {
        this.passengerCreatedEventData = passengerCreatedEventData;
    }

    public PassengerCreatedEventData getPassengerCreatedEventData() {
        return passengerCreatedEventData;
    }
}
