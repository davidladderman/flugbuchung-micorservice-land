package com.llstudios.flightbookingservice.domain.passenger.aggregates;

import com.llstudios.flightbookingservice.domain.passenger.ValueObjects.Geld;
import com.llstudios.flightbookingservice.domain.passenger.commands.AddMoneyCommand;
import com.llstudios.flightbookingservice.domain.passenger.commands.CreatePassengerCommand;
import com.llstudios.flightbookingservice.domain.passenger.commands.DecreasePassengerMoneyCommand;
import lombok.Data;
import org.springframework.data.domain.AbstractAggregateRoot;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@Entity
@Data
public class Passenger extends AbstractAggregateRoot<Passenger> {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(unique = true)
    private String passnummer;

    @Basic
    private String firstName;
    @Basic
    private String lastName;
    @Basic
    private String postleitzahl;
    @Basic
    private String ort;

    @Embedded
    private Geld passengerGeld;

    public Passenger() {
    }

    public Passenger(CreatePassengerCommand createPassengerCommand){
        this.passnummer = createPassengerCommand.getPassnummer();
        this.firstName = createPassengerCommand.getFirstName();
        this.lastName = createPassengerCommand.getLastName();
        this.ort = createPassengerCommand.getOrt();
        this.postleitzahl = createPassengerCommand.getPostleitzahl();
    }


    public void addMoneyBudget(AddMoneyCommand addMoneyCommand) {
        if (this.passengerGeld != null){
            this.passengerGeld = new Geld(this.passengerGeld.getGeld().add(new BigDecimal(addMoneyCommand.getGeld())));
        }
        else {
            this.passengerGeld = new Geld(new BigDecimal(addMoneyCommand.getGeld()));
        };
    }
    public void decreaseMoneyBudget(DecreasePassengerMoneyCommand decreasePassengerMoneyCommand) {
        this.passengerGeld = new Geld(this.passengerGeld.getGeld().subtract(new BigDecimal(decreasePassengerMoneyCommand.getGeld())));//minus machen
    }
}
