package com.llstudios.flightbookingservice.domain.booking.aggregates;

import com.llstudios.flightbookingservice.domain.booking.commands.CreateBookingCommand;
import lombok.Data;
import org.springframework.data.domain.AbstractAggregateRoot;

import javax.persistence.*;
import java.sql.Date;
import java.sql.Timestamp;

@Entity
@Data
public class Booking extends AbstractAggregateRoot<Booking> {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(unique = true)
    private String bookingnummer;
    private String flugnummer;
    private String passnummer;
    private Timestamp bookingtime;

    public Booking() {
    }

    public Booking(CreateBookingCommand createBookingCommand){
        this.bookingnummer = createBookingCommand.getBookingnummer();
        this.flugnummer = createBookingCommand.getFlugnummer();
        this.passnummer = createBookingCommand.getPassnummer();
        this.bookingtime = new Timestamp(System.currentTimeMillis());
    }
}
