package com.llstudios.flightbookingservice.domain.flight.aggregates;

import com.llstudios.flightbookingservice.domain.flight.ValueObjects.Cost;
import com.llstudios.flightbookingservice.domain.flight.commands.CreateFlightCommand;
import com.llstudios.flightbookingservice.domain.flight.commands.AddCostCommand;
import com.llstudios.flightbookingservice.shareddomain.FlightCreatedEvent;
import com.llstudios.flightbookingservice.shareddomain.FlightCreatedEventData;
import com.sun.istack.NotNull;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import org.springframework.data.domain.AbstractAggregateRoot;

import javax.persistence.*;
import java.math.BigDecimal;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;


@Entity
@Getter
@EqualsAndHashCode
public class Flight extends AbstractAggregateRoot<Flight> {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @Column(unique = true)
    private String flightNumber;

    @Basic
    private String flightFlugzeitBis;

    @Basic
    private String flightFlugzeitvon;

    @Basic
    private String flightdate;

    @Basic
    private String flightStartflughafen;

    @Basic
    private String flightZielflughafen;

    @Embedded
    private Cost flightCost;

    protected Flight() {

    }

    public Flight(CreateFlightCommand createFlightCommand) {
        this.flightNumber = createFlightCommand.getFlightNumber();
        this.flightdate = createFlightCommand.getFlightDate();
        this.flightFlugzeitBis = createFlightCommand.getFlightFlugzeitBis();
        this.flightFlugzeitvon = createFlightCommand.getFlightFlugzeitVon();
        this.flightStartflughafen = createFlightCommand.getFlightStartflughafen();
        this.flightZielflughafen = createFlightCommand.getFlightZielflughafen();
    }

    public void addCost(AddCostCommand addCostCommand) {
        this.flightCost = new Cost(new BigDecimal(addCostCommand.getCost()));
    }



}
