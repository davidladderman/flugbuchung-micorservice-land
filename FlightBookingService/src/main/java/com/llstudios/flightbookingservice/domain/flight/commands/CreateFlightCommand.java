package com.llstudios.flightbookingservice.domain.flight.commands;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Value;

@Value
@AllArgsConstructor
public class CreateFlightCommand {
    String flightDate;
    String flightFlugzeitBis;
    String flightFlugzeitVon;
    String flightNumber;
    String flightStartflughafen;
    String flightZielflughafen;
}
