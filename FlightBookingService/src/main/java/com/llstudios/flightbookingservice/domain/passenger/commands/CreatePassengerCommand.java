package com.llstudios.flightbookingservice.domain.passenger.commands;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Value;

@Value
@Builder
public class CreatePassengerCommand {
    private String firstName;
    private String lastName;
    private String postleitzahl;
    private String ort;
    private String passnummer;

    public CreatePassengerCommand(String firstName, String lastName, String postleitzahl, String ort, String passnummer) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.postleitzahl = postleitzahl;
        this.ort = ort;
        this.passnummer = passnummer;
    }
}
