package com.llstudios.flightbookingservice.domain.flight.commands;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Value;

@Value
@AllArgsConstructor
@Builder
public class AddCostCommand {
    String flightnumber;
    String cost;
}
