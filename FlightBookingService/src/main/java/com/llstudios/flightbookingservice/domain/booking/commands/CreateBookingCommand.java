package com.llstudios.flightbookingservice.domain.booking.commands;

import lombok.AllArgsConstructor;
import lombok.Value;

@Value
@AllArgsConstructor
public class CreateBookingCommand {
    private String bookingnummer;
    private String passnummer;
    private String flugnummer;
}
