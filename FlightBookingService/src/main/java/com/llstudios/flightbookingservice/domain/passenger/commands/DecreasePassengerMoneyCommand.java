package com.llstudios.flightbookingservice.domain.passenger.commands;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Value;

@Value
@AllArgsConstructor
@Builder
public class DecreasePassengerMoneyCommand {
    String passnummer;
    String geld;
}
