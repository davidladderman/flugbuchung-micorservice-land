package com.llstudios.flightservice.services;

import com.llstudios.flightservice.infrastructure.messagebroker.FlightChannels;
import com.llstudios.flightservice.shareddomain.events.FlightCreatedEvent;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.event.TransactionalEventListener;

@Service
@EnableBinding(FlightChannels.class)
@Slf4j
@AllArgsConstructor
public class FlightService {

    //Injektion der passenden Instanz druch Spring -> StudentChannels = Binder zwischen Spring Cloud Stream und RabbitMQ-Implementierung
    private final FlightChannels flightChannels;


    @TransactionalEventListener
    public void handleFlightCreatedEvent(FlightCreatedEvent flightCreatedEvent) {
        try {
            log.info("Listend to Domain Event -> Student Created: " + flightCreatedEvent.getFlightCreatedEventData());
            log.info("Send Domain Event Data to Message Broker Queue");
            flightChannels.flightCreationSource().send(MessageBuilder.withPayload(flightCreatedEvent).build());
        } catch (Exception ex) {
            log.error("Problem with connection to message broker. Could not send Student Created Event: " + flightCreatedEvent.getFlightCreatedEventData());
            ex.printStackTrace();
        }
    }
}
