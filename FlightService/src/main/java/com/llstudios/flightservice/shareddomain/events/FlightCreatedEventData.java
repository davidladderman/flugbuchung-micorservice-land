package com.llstudios.flightservice.shareddomain.events;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
public class FlightCreatedEventData {
    private String flightDate;
    private String flightFlugzeitBis;
    private String flightFlugzeitVon;
    private String flightNumber;
    private String flightStartflughafen;
    private String flightZielflughafen;

    public FlightCreatedEventData() {
    }

    public FlightCreatedEventData(String flightDate, String flightFlugzeitBis, String flightFlugzeitVon, String flightNumber, String flightStartflughafen, String flightZielflughafen) {
        this.flightDate = flightDate;
        this.flightFlugzeitBis = flightFlugzeitBis;
        this.flightFlugzeitVon = flightFlugzeitVon;
        this.flightNumber = flightNumber;
        this.flightStartflughafen = flightStartflughafen;
        this.flightZielflughafen = flightZielflughafen;
    }

}