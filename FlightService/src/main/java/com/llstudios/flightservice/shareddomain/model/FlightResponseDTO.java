package com.llstudios.flightservice.shareddomain.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class FlightResponseDTO {
    private String flightDate;
    private String flightFlugzeitBis;
    private String flightFlugzeitVon;
    private String flightNumber;
    private String flightStartflughafen;
    private String flightZielflughafen;
}
