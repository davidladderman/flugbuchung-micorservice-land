package com.llstudios.flightservice.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.EXPECTATION_FAILED)
public class FlightzeitBisNotValidException extends Exception {
    public FlightzeitBisNotValidException() {
        super("Invalid FlugzeitBis. Format is 23:59");
    }
}
