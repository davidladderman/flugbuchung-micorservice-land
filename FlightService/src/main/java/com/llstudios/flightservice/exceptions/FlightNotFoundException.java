package com.llstudios.flightservice.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class FlightNotFoundException extends Exception {

    public FlightNotFoundException() {
        super("Flight with given Number not found");
    }
}
