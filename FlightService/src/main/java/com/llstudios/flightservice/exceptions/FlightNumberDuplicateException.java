package com.llstudios.flightservice.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.CONFLICT)
public class FlightNumberDuplicateException extends Exception{

    public FlightNumberDuplicateException(){super("Flightnumber already exists."); }
}
