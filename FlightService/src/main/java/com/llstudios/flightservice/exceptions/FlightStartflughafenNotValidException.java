package com.llstudios.flightservice.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.EXPECTATION_FAILED)
public class FlightStartflughafenNotValidException extends Exception {
    public FlightStartflughafenNotValidException() {
        super("Invalid Course Name. At least 2 characters required");
    }
}
