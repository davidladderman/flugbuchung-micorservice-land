package com.llstudios.flightservice.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.EXPECTATION_FAILED)
public class FlightdatumNotValidException extends Exception {
    public FlightdatumNotValidException() {
        super("Invalid Flight Date. Format: dd/mm/yyyy");
    }
}
