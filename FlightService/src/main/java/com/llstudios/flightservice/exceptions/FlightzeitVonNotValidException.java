package com.llstudios.flightservice.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.EXPECTATION_FAILED)
public class FlightzeitVonNotValidException extends Exception {
    public FlightzeitVonNotValidException() {
        super("Invalid FlugzeitVon. Format is 23:59");
    }
}
