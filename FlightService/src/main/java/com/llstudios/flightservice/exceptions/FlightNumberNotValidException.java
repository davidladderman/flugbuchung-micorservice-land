package com.llstudios.flightservice.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.EXPECTATION_FAILED)
public class FlightNumberNotValidException extends Exception {
    public FlightNumberNotValidException() {
        super("Invalid flight number. 6 digits required");
    }
}
