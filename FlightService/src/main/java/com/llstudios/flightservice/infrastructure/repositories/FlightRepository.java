package com.llstudios.flightservice.infrastructure.repositories;

import com.llstudios.flightservice.domain.aggregates.Flight;
import com.llstudios.flightservice.domain.valueobjects.FlightNumber;
import org.springframework.data.jpa.repository.JpaRepository;

import javax.swing.text.html.Option;
import java.util.Optional;

public interface FlightRepository extends JpaRepository<Flight, Long> {
    Optional<Flight> findByFlightNumber(FlightNumber flightNumber);
}
