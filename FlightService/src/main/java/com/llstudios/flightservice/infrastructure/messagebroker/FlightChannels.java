package com.llstudios.flightservice.infrastructure.messagebroker;

import org.springframework.cloud.stream.annotation.Input;
import org.springframework.cloud.stream.annotation.Output;
import org.springframework.messaging.MessageChannel;

public interface FlightChannels {
    String FLIGHT_CREATION_CHANNEL_SOURCE = "flightCreationChannelSource";
    String FLIGHT_CREATION_CHANNEL_SINK = "flightCreationChannelSink";

    @Output(FLIGHT_CREATION_CHANNEL_SOURCE)
        //Name des Kanals für Spring Cloud Stream -> Verbindung zum RabbitMQ-Channel erfolgt über diesen Namen in den application.properties.
    MessageChannel flightCreationSource();

    @Input(FLIGHT_CREATION_CHANNEL_SINK)
    MessageChannel flightCreationSink();

}
