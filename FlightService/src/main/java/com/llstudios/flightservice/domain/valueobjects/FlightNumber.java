package com.llstudios.flightservice.domain.valueobjects;

import com.llstudios.flightservice.exceptions.FlightNumberNotValidException;
import lombok.EqualsAndHashCode;
import lombok.Getter;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Embeddable
@Getter
@EqualsAndHashCode
public class FlightNumber {

    @Size(min = 6, max = 6)
    @Column(unique = true)
    private String flightNumber = "000000";

    protected FlightNumber() {

    }

    public FlightNumber(String flightNumber) throws FlightNumberNotValidException {
        if (flightNumber.matches("^([0-9]{6})")) {
            this.flightNumber = flightNumber;
        } else {
            throw new FlightNumberNotValidException();
        }
    }
}
