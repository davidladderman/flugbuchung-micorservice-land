package com.llstudios.flightservice.domain.valueobjects;

import com.llstudios.flightservice.exceptions.FlightZielflughafenNotValidException;
import lombok.EqualsAndHashCode;
import lombok.Getter;

import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Embeddable
@Getter
@EqualsAndHashCode
public class FlightZielflughafen {

    @NotNull
    @Size(min = 2)
    private String flightZielflughafen = "Ziel";

    protected FlightZielflughafen() {
    }

    public FlightZielflughafen(String flightZielflughafen) throws FlightZielflughafenNotValidException {
        if (flightZielflughafen != null && flightZielflughafen != "" && flightZielflughafen.length() > 1) {
            this.flightZielflughafen = flightZielflughafen;
        } else {
            throw new FlightZielflughafenNotValidException();
        }
    }
}
