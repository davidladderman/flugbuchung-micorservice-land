package com.llstudios.flightservice.domain.valueobjects;

import com.llstudios.flightservice.exceptions.FlightzeitBisNotValidException;
import lombok.EqualsAndHashCode;
import lombok.Getter;

import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

@Embeddable
@Getter
@EqualsAndHashCode
public class FlightDate {

    @NotNull
    @Size(min = 2)
    private String flightdate = "";

    protected FlightDate() {
    }

    public FlightDate(String flightdate) throws FlightzeitBisNotValidException, ParseException {
        if (flightdate.trim().equals("")) {
            throw new FlightzeitBisNotValidException();
        } else {
            SimpleDateFormat sdfrmt = new SimpleDateFormat("dd/mm/yyyy");
            try {
                Date date = sdfrmt.parse(flightdate);
                this.flightdate = flightdate;
            }
            catch (ParseException e)
            {
                System.out.println(flightdate+" is not a valid Date format.");
            }
        }
    }
}
