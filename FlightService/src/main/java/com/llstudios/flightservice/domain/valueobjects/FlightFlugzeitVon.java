package com.llstudios.flightservice.domain.valueobjects;

import com.llstudios.flightservice.exceptions.FlightzeitVonNotValidException;
import lombok.EqualsAndHashCode;
import lombok.Getter;

import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.text.ParseException;

@Embeddable
@Getter
@EqualsAndHashCode
public class FlightFlugzeitVon {


    @NotNull
    @Size(min = 2)
    private String flightFlugzeitvon = "00:00";

    protected FlightFlugzeitVon() {
    }

    public FlightFlugzeitVon(String flightFlugzeitvon) throws FlightzeitVonNotValidException, ParseException {
        if (flightFlugzeitvon.matches("^([0-9]|0[0-9]|1[0-9]|2[0-3]):[0-5][0-9]$")) {
            this.flightFlugzeitvon = flightFlugzeitvon;
        } else {
            throw new FlightzeitVonNotValidException();
        }
    }
}
