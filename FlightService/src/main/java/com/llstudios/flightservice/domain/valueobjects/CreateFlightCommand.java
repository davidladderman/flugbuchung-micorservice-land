package com.llstudios.flightservice.domain.valueobjects;

import lombok.AllArgsConstructor;
import lombok.Value;

@Value
@AllArgsConstructor
public class CreateFlightCommand {
    String flightDate;
    String flightFlugzeitBis;
    String flightFlugzeitVon;
    String flightNumber;
    String flightStartflughafen;
    String flightZielflughafen;
}
