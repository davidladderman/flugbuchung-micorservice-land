package com.llstudios.flightservice.domain.aggregates;

import com.llstudios.flightservice.domain.valueobjects.*;
import com.llstudios.flightservice.exceptions.*;
import com.llstudios.flightservice.shareddomain.events.FlightCreatedEvent;
import com.llstudios.flightservice.shareddomain.events.FlightCreatedEventData;
import com.sun.istack.NotNull;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.domain.AbstractAggregateRoot;

import javax.persistence.*;
import java.text.ParseException;

@Entity
@Getter
@Setter
@EqualsAndHashCode
public class Flight extends AbstractAggregateRoot<Flight> {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @Embedded
    @Column(unique = true)
    private FlightNumber flightNumber;

    @Embedded
    @NotNull
    private FlightFlugzeitBis flightFlugzeitBis;

    @Embedded
    @NotNull
    private FlightFlugzeitVon flightFlugzeitvon;

    @Embedded
    @NotNull
    private FlightDate flightdate;

    @Embedded
    @NotNull
    private FlightStartflughafen flightStartflughafen;

    @Embedded
    @NotNull
    private FlightZielflughafen flightZielflughafen;

    protected Flight() {

    }

    public Flight(CreateFlightCommand createFlightCommand) throws FlightNumberNotValidException, ParseException, FlightzeitBisNotValidException, FlightzeitVonNotValidException, FlightStartflughafenNotValidException, FlightZielflughafenNotValidException {
        this.flightNumber = new FlightNumber(createFlightCommand.getFlightNumber());
        this.flightdate = new FlightDate(createFlightCommand.getFlightDate());
        this.flightFlugzeitBis = new FlightFlugzeitBis(createFlightCommand.getFlightFlugzeitBis());
        this.flightFlugzeitvon = new FlightFlugzeitVon(createFlightCommand.getFlightFlugzeitVon());
        this.flightStartflughafen = new FlightStartflughafen(createFlightCommand.getFlightStartflughafen());
        this.flightZielflughafen = new FlightZielflughafen(createFlightCommand.getFlightZielflughafen());

        addDomainEvent(new FlightCreatedEvent(new FlightCreatedEventData(this.flightdate.getFlightdate(), this.flightFlugzeitBis.getFlightFlugzeitBis(),this.flightFlugzeitvon.getFlightFlugzeitvon(),this.flightNumber.getFlightNumber(),this.flightStartflughafen.getFlightStartflughafen(),this.flightZielflughafen.getFlightZielflughafen())));
    }

    private void addDomainEvent(Object event){
        registerEvent(event);
    }


}
