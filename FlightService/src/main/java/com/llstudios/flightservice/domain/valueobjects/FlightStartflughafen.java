package com.llstudios.flightservice.domain.valueobjects;

import com.llstudios.flightservice.exceptions.FlightStartflughafenNotValidException;
import lombok.EqualsAndHashCode;
import lombok.Getter;

import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Embeddable
@Getter
@EqualsAndHashCode
public class FlightStartflughafen {

    @NotNull
    @Size(min = 2)
    private String flightStartflughafen = "Start";

    protected FlightStartflughafen() {
    }

    public FlightStartflughafen(String flightStartflughafen) throws FlightStartflughafenNotValidException {
        if (flightStartflughafen != null && flightStartflughafen != "" && flightStartflughafen.length() > 1) {
            this.flightStartflughafen = flightStartflughafen;
        } else {
            throw new FlightStartflughafenNotValidException();
        }
    }
}
