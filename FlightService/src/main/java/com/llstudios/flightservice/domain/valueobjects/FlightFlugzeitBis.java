package com.llstudios.flightservice.domain.valueobjects;

import com.llstudios.flightservice.exceptions.FlightzeitBisNotValidException;
import lombok.EqualsAndHashCode;
import lombok.Getter;

import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.text.ParseException;

@Embeddable
@Getter
@EqualsAndHashCode
public class FlightFlugzeitBis {


    @NotNull
    @Size(min = 2)
    private String flightFlugzeitBis = "00:00";

    protected FlightFlugzeitBis() {
    }

    public FlightFlugzeitBis(String flightFlugzeitBis) throws FlightzeitBisNotValidException, ParseException {
        if (flightFlugzeitBis.matches("^([0-9]|0[0-9]|1[0-9]|2[0-3]):[0-5][0-9]$")) {
            this.flightFlugzeitBis = flightFlugzeitBis;
        } else {
            throw new FlightzeitBisNotValidException();
        }
    }
}
