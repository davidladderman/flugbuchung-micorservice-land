package com.llstudios.flightservice.api.rest.Mapper;

import com.llstudios.flightservice.api.rest.CreateFlightDTO;
import com.llstudios.flightservice.domain.valueobjects.CreateFlightCommand;

public class FlightDTOtoFlightCommandMapper {
    public static CreateFlightCommand toCreateFlightCommand(CreateFlightDTO createFlightDTO) {
        return new CreateFlightCommand(
                createFlightDTO.getFlightDate(),
                createFlightDTO.getFlightFlugzeitBis(),
                createFlightDTO.getFlightFlugzeitVon(),
                createFlightDTO.getFlightNumber(),
                createFlightDTO.getFlightStartflughafen(),
                createFlightDTO.getFlightZielflughafen()
                );
    }
}


