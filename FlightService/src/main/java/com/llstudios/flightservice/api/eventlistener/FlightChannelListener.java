package com.llstudios.flightservice.api.eventlistener;

import com.llstudios.flightservice.infrastructure.messagebroker.FlightChannels;
import com.llstudios.flightservice.shareddomain.events.FlightCreatedEvent;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.annotation.StreamListener;

@EnableBinding(FlightChannels.class)
@Slf4j
public class FlightChannelListener {

    @StreamListener(target = FlightChannels.FLIGHT_CREATION_CHANNEL_SINK)
    //Nur für Testzwecke
    public void processCourseCreated(FlightCreatedEvent flightCreatedEvent) {
        log.info("Horcht aufn Broker -> Flight Created Event: " + flightCreatedEvent.getFlightCreatedEventData());
    }
}
