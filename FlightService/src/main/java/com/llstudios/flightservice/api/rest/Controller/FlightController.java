package com.llstudios.flightservice.api.rest.Controller;

import com.llstudios.flightservice.api.rest.CreateFlightDTO;
import com.llstudios.flightservice.api.rest.Mapper.FlightDTOtoFlightCommandMapper;
import com.llstudios.flightservice.domain.aggregates.Flight;
import com.llstudios.flightservice.domain.valueobjects.FlightNumber;
import com.llstudios.flightservice.exceptions.*;
import com.llstudios.flightservice.infrastructure.repositories.FlightRepository;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.text.ParseException;
import java.util.Optional;

@RestController
@RequestMapping("/flights")
public class FlightController {

    private final FlightRepository flightRepository;

    public FlightController(FlightRepository flightRepository){
        this.flightRepository = flightRepository;
    }


    @PostMapping
    public ResponseEntity<?> createFlight(@RequestBody CreateFlightDTO createFlightDTO) throws FlightzeitVonNotValidException, ParseException, FlightzeitBisNotValidException, FlightStartflughafenNotValidException, FlightZielflughafenNotValidException, FlightNumberNotValidException {
        System.out.println("Post Mapping fürn Flight");
        Flight flight = new Flight(FlightDTOtoFlightCommandMapper.toCreateFlightCommand(createFlightDTO));
        flightRepository.save(flight);
        return ResponseEntity.status(201).build();
    }

    @GetMapping("/{flightnumber}")
    public Optional<Flight> getCourseByNumer(@PathVariable FlightNumber flightnumber) {
        System.out.println("Get Mapping fürn Flight");
        return flightRepository.findByFlightNumber(flightnumber);
    }

    @DeleteMapping("/delete/{flightnumber}")
    public void deleteFlight(@PathVariable Long id){
        flightRepository.deleteById(id);
    }

    @PutMapping("/update/{id}")
    public Flight updateFlight(@RequestBody Flight newFlight, @PathVariable Long id){
        return flightRepository.findById(id).map(flight -> {
            flight.setFlightdate(newFlight.getFlightdate());
            flight.setFlightFlugzeitBis(newFlight.getFlightFlugzeitBis());
            flight.setFlightFlugzeitvon(newFlight.getFlightFlugzeitvon());
            flight.setFlightNumber(newFlight.getFlightNumber());
            flight.setFlightStartflughafen(newFlight.getFlightStartflughafen());
            flight.setFlightZielflughafen(newFlight.getFlightZielflughafen());
            return flightRepository.save(flight);
        }).orElseGet(() ->{
            newFlight.setId(id);
            return flightRepository.save(newFlight);
        });
    }
}
