package com.llstudios.flightservice.api.rest;

import lombok.AllArgsConstructor;
import lombok.Value;

@Value
@AllArgsConstructor
public class CreateFlightDTO {
    private String flightDate;
    private String flightFlugzeitBis;
    private String flightFlugzeitVon;
    private String flightNumber;
    private String flightStartflughafen;
    private String flightZielflughafen;
}
